package tech.iooo.coco.client.utils;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.core.URL;

/**
 * Created on 2018-12-12 10:56
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class UrlTest {

  private static final Logger logger = LoggerFactory.getLogger(UrlTest.class);

  @Test
  void test() {
    URL url = URL.valueOf("http://85.17.73.31");
    logger.info("url {}", url);
    System.out.println(URL.valueOf("|http://85.17.73.31".substring(1)));
  }
}
