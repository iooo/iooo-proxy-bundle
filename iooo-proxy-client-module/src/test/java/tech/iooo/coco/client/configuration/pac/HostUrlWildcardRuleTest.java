package tech.iooo.coco.client.configuration.pac;

import org.junit.jupiter.api.Test;
import tech.iooo.boot.core.URL;

/**
 * Created on 2018-12-11 13:48
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
class HostUrlWildcardRuleTest {

  @Test
  public void test() {
    URL url = URL.valueOf("*.iooo.tech/");
    System.out.println(url);
    System.out.println(url.getHost());
  }
}
