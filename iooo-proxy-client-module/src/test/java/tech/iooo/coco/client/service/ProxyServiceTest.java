package tech.iooo.coco.client.service;

import java.util.regex.Pattern;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.coco.client.configuration.pac.ProxyRule;

/**
 * Created on 2018-12-11 14:38
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class ProxyServiceTest {

  private static final Logger logger = LoggerFactory.getLogger(ProxyServiceTest.class);

  private static ProxyRule proxyRule;

  @BeforeAll
  public static void before() {
    proxyRule = ProxyRule.getInstance();
    ProxyRule.init();
  }


  @Test
  public void proxy() {
    System.out.println("proxyList");
    System.out.println(ProxyRule.proxyList);
    System.out.println("whiteList");
    System.out.println(ProxyRule.whiteList);

    System.out.println("Hello world".substring(2));

    logger.info("{}", proxyRule.filter("https://www.google.com/app/test"));
    logger.info("{}", proxyRule.filter("https://*.google.com"));
    logger.info("{}", proxyRule.filter(".google.com"));
    logger.info("{}", proxyRule.filter("https://*.baidu.com"));

    System.out.println(Pattern.compile(".*\\.freeddns\\.com.*").matcher("www.freeddns.com").matches());
  }
}
