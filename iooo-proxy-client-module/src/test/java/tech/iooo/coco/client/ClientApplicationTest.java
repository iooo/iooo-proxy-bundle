package tech.iooo.coco.client;

import java.nio.charset.StandardCharsets;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import tech.iooo.boot.core.io.Resources;


/**
 * Unit test for simple App.
 */
public class ClientApplicationTest {

  /**
   * Rigorous Test :-)
   */
  @Test
  @SneakyThrows
  public void shouldAnswerWithTrue() {
    String data = new String(Base64.decodeBase64(IOUtils.toString(Resources.getResourceAsStream("pac/gfwlist.txt"))), StandardCharsets.UTF_8);
    System.out.println(data);
  }
}
