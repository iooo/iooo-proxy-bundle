package tech.iooo.coco.client.configuration.service;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.core.io.Resources;

/**
 * Created on 2018-12-10 22:04
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
class ServiceConfigTest {

  private static final Logger logger = LoggerFactory.getLogger(ServiceConfigTest.class);
  private Gson gson = new Gson();

  @Test
  @SneakyThrows
  public void test() {
    ServiceConfig serviceConfig = gson.fromJson(IOUtils.toString(Resources.getResourceAsStream("config.json")), ServiceConfig.class);
    System.out.println(serviceConfig);
  }
}
