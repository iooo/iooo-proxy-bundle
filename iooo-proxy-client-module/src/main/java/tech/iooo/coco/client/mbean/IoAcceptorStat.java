package tech.iooo.coco.client.mbean;


import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Ivan97
 */
@Service
public class IoAcceptorStat implements IoAcceptorStatMBean {

  @Autowired
  private GlobalTrafficShapingHandler trafficCounter;

  @Override
  public long getWrittenBytesThroughput() {
    return trafficCounter.trafficCounter().lastWriteThroughput();
  }

  @Override
  public long getReadBytesThroughput() {
    return trafficCounter.trafficCounter().lastReadThroughput();
  }
}
