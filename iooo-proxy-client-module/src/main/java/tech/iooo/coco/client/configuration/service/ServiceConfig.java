package tech.iooo.coco.client.configuration.service;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.Generated;
import lombok.Data;

@Data
@Generated("com.robohorse.robopojogenerator")
public class ServiceConfig {

  @SerializedName("local-port")
  private int localPort = 6155;
  
  @SerializedName("global")
  private boolean globalMode = false;

  @SerializedName("servers")
  private List<ServersItem> servers;
}
