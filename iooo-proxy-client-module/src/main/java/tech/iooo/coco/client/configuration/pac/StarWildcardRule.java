package tech.iooo.coco.client.configuration.pac;

import java.util.regex.Pattern;
import tech.iooo.coco.client.utils.RuleHelper;

/**
 * Created on 2019-02-20 13:52
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class StarWildcardRule extends AbstractProxyRule {
  
  private Pattern ruleRex;

  @Override
  boolean init(String rule) {
    ruleRex = RuleHelper.prepareRegexPattern(rule);
    return true;
  }

  @Override
  boolean match(String host) {
    return ruleRex.matcher(host).matches();
  }
}
