package tech.iooo.coco.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.iooo.coco.client.configuration.pac.ProxyRule;
import tech.iooo.coco.client.configuration.service.ServiceConfig;

/**
 * Created on 2018-12-10 21:36
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Service
public class ProxyService {

  @Autowired
  private ProxyRule proxyRule;

  @Autowired
  private ServiceConfig serviceConfig;

  public synchronized boolean proxy(String host) {
    return proxyRule.filter(host);
  }

  public boolean isGlobalMode() {
    return serviceConfig.isGlobalMode();
  }
}
