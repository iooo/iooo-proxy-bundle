package tech.iooo.coco.client.lifecycle;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import java.net.InetSocketAddress;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.SmartLifecycle;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import tech.iooo.boot.netty.handler.NettyAdaptiveHelper;
import tech.iooo.coco.client.configuration.service.ServiceConfig;
import tech.iooo.coco.client.mbean.IoAcceptorStat;
import tech.iooo.coco.client.proxy.SocksServerInitializer;
import tech.iooo.coco.client.utils.SpringUtils;

/**
 * Created on 2018-12-10 21:03
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Component
public class ProxyClientLifecycle implements SmartLifecycle, ApplicationContextAware {

  private static final Logger logger = LoggerFactory.getLogger(ProxyClientLifecycle.class);
  @Autowired
  private ServiceConfig serviceConfig;
  @Autowired
  private SocksServerInitializer socksServerInitializer;
  @Autowired
  private MBeanServer mBeanServer;
  @Autowired
  private IoAcceptorStat ioAcceptorStat;

  private EventLoopGroup bossGroup = NettyAdaptiveHelper.bossEventLoopGroup;
  private EventLoopGroup workerGroup = NettyAdaptiveHelper.clientEventLoopGroup(5);

  private boolean running;

  @Override
  @SneakyThrows
  public void start() {

    ServerBootstrap serverBootstrap = new ServerBootstrap();

    serverBootstrap.group(bossGroup, workerGroup)
        .channel(NettyAdaptiveHelper.serverSocketChannel)
        .localAddress(new InetSocketAddress(serviceConfig.getLocalPort()))
        .childHandler(new ChannelInitializer<SocketChannel>() {
          @Override
          protected void initChannel(SocketChannel socketChannel) throws Exception {
            ChannelPipeline channelPipeline = socketChannel.pipeline();
            channelPipeline.addLast("logger", new LoggingHandler(LogLevel.DEBUG));
            channelPipeline.addLast("socketServer", socksServerInitializer);
          }
        });
    startMBean();
    ChannelFuture future = serverBootstrap.bind().sync().addListener(result -> {
      if (result.isSuccess()) {
        logger.info("services started on port {}", serviceConfig.getLocalPort());
      }
    });
    future.channel().closeFuture().sync();

    this.running = true;
  }

  @Override
  public void stop() {
    bossGroup.shutdownGracefully().syncUninterruptibly();
    workerGroup.shutdownGracefully().syncUninterruptibly();
    this.running = false;
  }

  /**
   * Java MBean 进行流量统计
   */
  private void startMBean() {
    try {
      ObjectName acceptorName = new ObjectName(ioAcceptorStat.getClass().getPackage().getName() + ":type=IoAcceptorStat");
      mBeanServer.registerMBean(ioAcceptorStat, acceptorName);
    } catch (Exception e) {
      logger.error("Java MBean error", e);
    }
  }

  @Override
  public boolean isRunning() {
    return this.running;
  }

  @Override
  public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
    SpringUtils.init(applicationContext);
  }
}
