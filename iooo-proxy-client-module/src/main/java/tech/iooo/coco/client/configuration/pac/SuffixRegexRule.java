package tech.iooo.coco.client.configuration.pac;

/**
 * Created on 2019-02-20 13:40
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class SuffixRegexRule extends AbstractProxyRule {

  private String suffix;

  @Override
  boolean init(String rule) {
    suffix = rule;
    return true;
  }

  @Override
  boolean match(String host) {
    return host.endsWith(suffix);
  }
}
