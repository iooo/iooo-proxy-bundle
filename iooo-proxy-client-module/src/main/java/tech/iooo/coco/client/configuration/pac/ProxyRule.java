package tech.iooo.coco.client.configuration.pac;

import static java.util.stream.Collectors.groupingBy;
import static tech.iooo.boot.core.constants.SystemProperties.LINE_SEPARATOR;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.map.LRUMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.core.io.Resources;
import tech.iooo.boot.core.utils.Assert;

/**
 * Created on 2018-12-11 14:19
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class ProxyRule extends SpacFilter {

  private static final Logger logger = LoggerFactory.getLogger(ProxyRule.class);
  public static List<AbstractProxyRule> whiteList = Lists.newArrayList();
  public static List<AbstractProxyRule> proxyList = Lists.newArrayList();
  private static ProxyRule instance = new ProxyRule();

  private volatile LRUMap<String, Boolean> cachedRule = new LRUMap<>(1000);

  private ProxyRule() {
  }

  public static ProxyRule getInstance() {
    return instance;
  }

  @SneakyThrows
  public static void init() {
    String data = new String(Base64.decodeBase64(IOUtils.toString(Resources.getResourceAsStream("pac/gfwlist.txt"))), StandardCharsets.UTF_8);
    List<String> pac = Arrays.asList(data.split(LINE_SEPARATOR));

    //根据是否为例外规则分组
    Map<Boolean, List<String>> rules = pac.stream()
        .filter(line -> !Strings.isNullOrEmpty(line))
        .filter(line -> !line.startsWith("!") && !line.startsWith("["))
        .collect(groupingBy(line -> line.startsWith("@@")));

    //白名单
    rules.get(true)
        .stream()
        .map(line -> line.replaceFirst("@@", ""))
        .forEach(line -> {
          if (line.startsWith("||")) {
            HostUrlWildcardRule rule = new HostUrlWildcardRule();
            rule.init(line.substring(2));
            whiteList.add(rule);
          } else if (line.startsWith("|")) {
            PrefixRegexRule rule = new PrefixRegexRule();
            Assert.isTrue(!Strings.isNullOrEmpty(line.substring(1)), "line " + line);
            rule.init(line.substring(1));
            whiteList.add(rule);
          } else if (line.endsWith("|")) {
            SuffixRegexRule rule = new SuffixRegexRule();
            Assert.isTrue(!Strings.isNullOrEmpty(line.substring(0, line.length() - 1)), "line " + line);
            rule.init(line.substring(0, line.length() - 1));
            whiteList.add(rule);
          } else if (line.startsWith("\\") && line.endsWith("\\")) {
            UrlRegexRule rule = new UrlRegexRule();
            rule.init(line.substring(1, line.length() - 1));
            whiteList.add(rule);
          } else {
            StarWildcardRule rule = new StarWildcardRule();
            rule.init(line);
            whiteList.add(rule);
          }
        });

    //代理名单
    rules.get(false)
        .forEach(line -> {
          if (line.startsWith("||")) {
            HostUrlWildcardRule rule = new HostUrlWildcardRule();
            rule.init(line.substring(2));
            proxyList.add(rule);
          } else if (line.startsWith("|")) {
            PrefixRegexRule rule = new PrefixRegexRule();
            Assert.isTrue(!Strings.isNullOrEmpty(line.substring(1)), "line " + line);
            rule.init(line.substring(1));
            proxyList.add(rule);
          } else if (line.endsWith("|")) {
            SuffixRegexRule rule = new SuffixRegexRule();
            Assert.isTrue(!Strings.isNullOrEmpty(line.substring(0, line.length() - 1)), "line " + line);
            rule.init(line.substring(0, line.length() - 1));
            proxyList.add(rule);
          } else if (line.startsWith("\\") && line.endsWith("\\")) {
            UrlRegexRule rule = new UrlRegexRule();
            rule.init(line.substring(1, line.length() - 1));
            proxyList.add(rule);
          } else {
            StarWildcardRule rule = new StarWildcardRule();
            rule.init(line);
            proxyList.add(rule);
          }
        });
    StarWildcardRule rule = new StarWildcardRule();
    rule.init("104.28.13.103");
    proxyList.add(rule);
  }

  @Override
  public boolean filter(String host) {
    if (cachedRule.keySet().contains(host)) {
      return cachedRule.get(host);
    }
    for (AbstractProxyRule rule : whiteList) {
      if (Objects.nonNull(rule) && rule.match(host)) {
        cachedRule.put(host, false);
        return false;
      }
    }
    for (AbstractProxyRule rule : proxyList) {
      if (Objects.nonNull(rule) && rule.match(host)) {
        cachedRule.put(host, true);
        return true;
      }
    }
    return false;
  }
}
