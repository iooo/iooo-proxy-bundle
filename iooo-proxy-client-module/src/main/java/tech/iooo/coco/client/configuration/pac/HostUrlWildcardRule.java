package tech.iooo.coco.client.configuration.pac;

import java.util.Objects;
import tech.iooo.boot.core.URL;
import tech.iooo.coco.client.utils.RuleHelper;

/**
 * Created on 2018-12-11 10:50
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class HostUrlWildcardRule extends AbstractProxyRule {

  private URL urlRule;

  @Override
  public boolean init(String rule) {
    this.urlRule = URL.valueOf(rule);
    return true;
  }

  @Override
  public boolean match(String host) {
    if (Objects.isNull(urlRule)) {
      return false;
    }
    return RuleHelper.wildCardMatch(host, urlRule.toString());
  }
}
