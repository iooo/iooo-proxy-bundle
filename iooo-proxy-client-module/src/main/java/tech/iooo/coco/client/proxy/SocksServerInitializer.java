package tech.iooo.coco.client.proxy;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.socks.SocksInitRequestDecoder;
import io.netty.handler.codec.socks.SocksMessageEncoder;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public final class SocksServerInitializer extends ChannelInitializer<SocketChannel> implements InitializingBean {

  @Autowired
  private GlobalTrafficShapingHandler trafficHandler;
  private SocksMessageEncoder socksMessageEncoder;

  @Override
  public void initChannel(SocketChannel socketChannel) throws Exception {
    ChannelPipeline p = socketChannel.pipeline();
    p.addLast(new SocksInitRequestDecoder());
    p.addLast(socksMessageEncoder);
    p.addLast(new SocksServerHandler());
    p.addLast(trafficHandler);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    socksMessageEncoder = new SocksMessageEncoder();
  }
}
