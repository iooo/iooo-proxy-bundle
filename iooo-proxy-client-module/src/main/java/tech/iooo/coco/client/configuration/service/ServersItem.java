package tech.iooo.coco.client.configuration.service;

import com.google.gson.annotations.SerializedName;
import javax.annotation.Generated;
import lombok.Data;

@Data
@Generated("com.robohorse.robopojogenerator")
public class ServersItem {

  @SerializedName("password")
  private String password;

  @SerializedName("method")
  private String method;

  @SerializedName("port")
  private int port;

  @SerializedName("host")
  private String host;

  @SerializedName("timeout")
  private long timeout;

  /**
   * 状态 true:可用 false:不可用
   **/
  private boolean available;
}
