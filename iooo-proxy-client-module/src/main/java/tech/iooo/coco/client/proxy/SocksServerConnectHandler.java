package tech.iooo.coco.client.proxy;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.socks.SocksAddressType;
import io.netty.handler.codec.socks.SocksCmdRequest;
import io.netty.handler.codec.socks.SocksCmdResponse;
import io.netty.handler.codec.socks.SocksCmdStatus;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.Promise;
import java.io.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.netty.handler.NettyAdaptiveHelper;
import tech.iooo.coco.client.configuration.service.ServersItem;
import tech.iooo.coco.client.manager.RemoteServerManager;
import tech.iooo.coco.client.service.ProxyService;
import tech.iooo.coco.client.utils.SpringUtils;
import tech.iooo.coco.core.encryption.CryptFactory;
import tech.iooo.coco.core.encryption.ICrypt;

/**
 * @author Ivan97
 */
@Sharable
public final class SocksServerConnectHandler extends SimpleChannelInboundHandler<SocksCmdRequest> {

  private static Logger logger = LoggerFactory.getLogger(SocksServerConnectHandler.class);
  private final Bootstrap bootstrap = new Bootstrap();
  private ProxyService proxyService;
  private ICrypt crypt;
  private ServersItem remoteServer;
  private boolean isProxy = true;

  public SocksServerConnectHandler() {
    this.proxyService = SpringUtils.instanceOf(ProxyService.class);
    this.remoteServer = SpringUtils.instanceOf(RemoteServerManager.class).getRemoteServer();
    this.crypt = CryptFactory.get(remoteServer.getMethod(), remoteServer.getPassword());
  }

  @Override
  public void channelRead0(final ChannelHandlerContext ctx, final SocksCmdRequest request) throws Exception {
    Promise<Channel> promise = ctx.executor().newPromise();
    setProxy(request.host());
    logger.info("host = " + request.host() + ",port = " + request.port() + ",isProxy = " + isProxy);
    promise.addListener(new GenericFutureListener<Future<Channel>>() {
      @Override
      public void operationComplete(final Future<Channel> future) throws Exception {
        final Channel outboundChannel = future.getNow();
        if (future.isSuccess()) {
          final InRelayHandler inRelay = new InRelayHandler(ctx.channel(), SocksServerConnectHandler.this);
          final OutRelayHandler outRelay = new OutRelayHandler(outboundChannel, SocksServerConnectHandler.this);

          ctx.channel().writeAndFlush(getSuccessResponse(request)).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) {
              ctx.pipeline().addLast("ss-logger", new LoggingHandler(LogLevel.DEBUG));

              try {
                if (isProxy) {
                  sendConnectRemoteMessage(request, outboundChannel);
                }

                ctx.pipeline().remove(SocksServerConnectHandler.this);
                outboundChannel.pipeline().addLast(inRelay);
                ctx.pipeline().addLast(outRelay);
              } catch (Exception e) {
                logger.error("", e);
              }
            }
          });
        } else {
          logger.error("channel error");
          ctx.channel().writeAndFlush(getFailureResponse(request));
          SocksServerUtils.closeOnFlush(ctx.channel());
        }
      }
    });

    final Channel inboundChannel = ctx.channel();
    bootstrap.group(inboundChannel.eventLoop())
        .channel(NettyAdaptiveHelper.clientSocketChannel)
        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
        .option(ChannelOption.SO_KEEPALIVE, true)
        .handler(new DirectClientHandler(promise));

    if (logger.isDebugEnabled()) {
      logger.debug("send request to {}:{}", getIpAddr(request), getPort(request));
    }

    bootstrap.connect(getIpAddr(request), getPort(request)).addListener(new ChannelFutureListener() {
      @Override
      public void operationComplete(ChannelFuture future) throws Exception {
        if (!future.isSuccess()) {
          ctx.channel().writeAndFlush(getFailureResponse(request));
          SocksServerUtils.closeOnFlush(ctx.channel());
        }
      }
    });
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    SocksServerUtils.closeOnFlush(ctx.channel());
  }

  public void setProxy(String host) {
    if (proxyService.isGlobalMode()) {
      isProxy = true;
    } else {
      isProxy = proxyService.proxy(host);
    }
  }

  /**
   * 获取远程ip地址
   */
  private String getIpAddr(SocksCmdRequest request) {
    if (isProxy) {
      return remoteServer.getHost();
    } else {
      return request.host();
    }
  }

  /**
   * 获取远程端口
   */
  private int getPort(SocksCmdRequest request) {
    if (isProxy) {
      return remoteServer.getPort();
    } else {
      return request.port();
    }
  }

  private SocksCmdResponse getSuccessResponse(SocksCmdRequest request) {
    return new SocksCmdResponse(SocksCmdStatus.SUCCESS, SocksAddressType.IPv4);
  }

  private SocksCmdResponse getFailureResponse(SocksCmdRequest request) {
    return new SocksCmdResponse(SocksCmdStatus.FAILURE, SocksAddressType.IPv4);
  }

  /**
   * localserver和remoteserver进行connect发送的数据
   */
  private void sendConnectRemoteMessage(SocksCmdRequest request, Channel outboundChannel) {
    ByteBuf buff = Unpooled.buffer();
    request.encodeAsByteBuf(buff);
    if (buff.hasArray()) {
      int len = buff.readableBytes();
      byte[] arr = new byte[len];
      buff.getBytes(0, arr);
      byte[] data = remoteByte(arr);
      sendRemote(data, data.length, outboundChannel);
    }
  }

  /**
   * localserver和remoteserver进行connect发送的数据
   *
   * +-----+-----+-------+------+----------+----------+ | VER | CMD | RSV | ATYP | DST.ADDR | DST.PORT | +-----+-----+-------+------+----------+----------+ | 1 | 1 | X'00' | 1 |
   * Variable | 2 | +-----+-----+-------+------+----------+----------+
   *
   * 需要跳过前面3个字节
   */
  private byte[] remoteByte(byte[] data) {
    int dataLength = data.length;
    dataLength -= 3;
    byte[] temp = new byte[dataLength];
    System.arraycopy(data, 3, temp, 0, dataLength);
    return temp;
  }

  /**
   * 给remoteserver发送数据--需要进行加密处理
   */
  public void sendRemote(byte[] data, int length, Channel channel) {
    try (ByteArrayOutputStream remoteOutStream = new ByteArrayOutputStream()) {
      if (isProxy) {
        crypt.encrypt(data, length, remoteOutStream);
        data = remoteOutStream.toByteArray();
      }
      channel.writeAndFlush(Unpooled.wrappedBuffer(data));
    } catch (Exception e) {
      logger.error("sendRemote error", e);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("sendRemote message:isProxy = " + isProxy + ",length = " + length + ",channel = " + channel);
    }
  }

  /**
   * 给本地客户端回复消息--需要进行解密处理
   */
  public void sendLocal(byte[] data, int length, Channel channel) {
    try (ByteArrayOutputStream localOutStream = new ByteArrayOutputStream()) {
      if (isProxy) {
        crypt.decrypt(data, length, localOutStream);
        data = localOutStream.toByteArray();
      }
      channel.writeAndFlush(Unpooled.wrappedBuffer(data));
    } catch (Exception e) {
      logger.error("sendLocal error", e);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("sendLocal message:isProxy = " + isProxy + ",length = " + length + ",channel = " + channel);
    }
  }
}
