package tech.iooo.coco.client.configuration.pac;

import java.util.regex.Pattern;
import tech.iooo.coco.client.utils.RuleHelper;

/**
 * Created on 2018-12-11 14:04
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public class UrlRegexRule extends AbstractProxyRule {

  private Pattern urlReg;

  @Override
  public boolean init(String rule) {
    urlReg = RuleHelper.prepareRegexPattern(rule);
    return true;
  }

  @Override
  public boolean match(String host) {
    return urlReg.matcher(host).matches();
  }
}
