package tech.iooo.coco.client.manager;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.net.telnet.TelnetClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.iooo.coco.client.configuration.service.ServersItem;
import tech.iooo.coco.client.configuration.service.ServiceConfig;

/**
 * 远程服务器管理器
 *
 * @author hui.zhao.cfs
 */
@Service
public class RemoteServerManager implements InitializingBean {

  private static Logger logger = LoggerFactory.getLogger(RemoteServerManager.class);
  private static Random random = new Random();
  private static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder()
      .namingPattern("i-manager-pool-%d")
      .daemon(true).build());

  @Autowired
  private ServiceConfig config;

  /**
   * 获取一台可用的远程服务器
   */
  public ServersItem getRemoteServer() {
    List<ServersItem> availableList = Lists.newArrayList();
    List<ServersItem> remoteList = config.getServers();
    for (ServersItem remoteServer : remoteList) {
      if (remoteServer.isAvailable()) {
        availableList.add(remoteServer);
      }
    }
    if (logger.isDebugEnabled()) {
      logger.debug("available remoteServer size = " + availableList.size());
    }
    if (availableList.size() > 0) {
      return availableList.get(random.nextInt(availableList.size()));
    }
    return remoteList.get(random.nextInt(remoteList.size()));
  }

  /**
   * 检测远程服务器是否可以连接
   */
  private void checkStatus() {
    List<ServersItem> remoteList = config.getServers();
    for (ServersItem remoteServer : remoteList) {
      if (isConnected(remoteServer)) {
        remoteServer.setAvailable(true);
      } else {
        remoteServer.setAvailable(false);
      }
    }
  }

  /**
   * telnet 检测是否能连通
   */
  private boolean isConnected(ServersItem remoteServer) {
    try {
      TelnetClient client = new TelnetClient();
      client.setDefaultTimeout(3000);
      client.connect(remoteServer.getHost(), remoteServer.getPort());
      return true;
    } catch (Exception e) {
      logger.warn("remote server: " + remoteServer.toString() + " telnet failed");
    }
    return false;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    checkStatus();
    scheduledThreadPoolExecutor.scheduleWithFixedDelay(() -> {
      try {
        checkStatus();
      } catch (Exception e) {
        logger.error("checkStatus error", e);
      }
    }, 30, 30, TimeUnit.SECONDS);
  }
}
