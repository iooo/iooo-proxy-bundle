package tech.iooo.coco.client.proxy;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.socks.SocksAuthResponse;
import io.netty.handler.codec.socks.SocksAuthScheme;
import io.netty.handler.codec.socks.SocksAuthStatus;
import io.netty.handler.codec.socks.SocksCmdRequest;
import io.netty.handler.codec.socks.SocksCmdRequestDecoder;
import io.netty.handler.codec.socks.SocksCmdType;
import io.netty.handler.codec.socks.SocksInitResponse;
import io.netty.handler.codec.socks.SocksRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Sharable
public final class SocksServerHandler extends SimpleChannelInboundHandler<SocksRequest> {

  private static Logger logger = LoggerFactory.getLogger(SocksServerHandler.class);

  @Override
  public void channelRead0(ChannelHandlerContext ctx, SocksRequest socksRequest) throws Exception {
    switch (socksRequest.requestType()) {
      case INIT: {
        if (logger.isDebugEnabled()) {
          logger.debug("local server init");
        }
        ctx.pipeline().addFirst(new SocksCmdRequestDecoder());
        ctx.write(new SocksInitResponse(SocksAuthScheme.NO_AUTH));
        break;
      }
      case AUTH:
        if (logger.isDebugEnabled()) {
          logger.debug("local server auth");
        }
        ctx.pipeline().addFirst(new SocksCmdRequestDecoder());
        ctx.write(new SocksAuthResponse(SocksAuthStatus.SUCCESS));
        break;
      case CMD:
        SocksCmdRequest req = (SocksCmdRequest) socksRequest;
        if (req.cmdType() == SocksCmdType.CONNECT) {
          if (logger.isDebugEnabled()) {
            logger.debug("local server connect");
          }
          ctx.pipeline().addLast(new SocksServerConnectHandler());
          ctx.pipeline().remove(this);
          ctx.fireChannelRead(socksRequest);
        } else {
          ctx.close();
        }
        break;
      case UNKNOWN:
      default:
        ctx.close();
        break;
    }
  }

  @Override
  public void channelReadComplete(ChannelHandlerContext ctx) {
    ctx.flush();
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable throwable) {
    throwable.printStackTrace();
    SocksServerUtils.closeOnFlush(ctx.channel());
  }
}
