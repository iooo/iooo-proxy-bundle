package tech.iooo.coco.client.configuration;

import com.google.gson.Gson;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import java.util.concurrent.ScheduledExecutorService;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.iooo.boot.core.io.Resources;
import tech.iooo.coco.client.configuration.pac.ProxyRule;
import tech.iooo.coco.client.configuration.service.ServiceConfig;

/**
 * Created on 2018-12-06 13:43
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Configuration
public class ProxyConfiguration {

  private static final String LINE_SEPARATOR = System.getProperty("line.separator");

  @Autowired
  private Gson gson;
  @Autowired
  private ScheduledExecutorService scheduledExecutorService;

  @Bean
  @SneakyThrows
  public ServiceConfig serviceConfig() {
    return gson.fromJson(IOUtils.toString(Resources.getResourceAsStream("config.json")), ServiceConfig.class);
  }

  @Bean
  @SneakyThrows
  public ProxyRule pacConfig() {
    ProxyRule.init();
    return ProxyRule.getInstance();
  }

  @Bean
  public GlobalTrafficShapingHandler trafficCounter() {
    return new GlobalTrafficShapingHandler(scheduledExecutorService, 1000);
  }
}
