package tech.iooo.coco.client.utils;

import lombok.experimental.UtilityClass;
import org.springframework.context.ApplicationContext;

/**
 * Created on 2019-02-21 14:45
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@UtilityClass
public class SpringUtils {

  private ApplicationContext applicationContext;

  public void init(ApplicationContext initialContext) {
    applicationContext = initialContext;
  }

  public <T> T instanceOf(Class<T> clazz) {
    return applicationContext.getBean(clazz);
  }
}
