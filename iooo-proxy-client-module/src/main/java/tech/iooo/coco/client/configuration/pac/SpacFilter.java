package tech.iooo.coco.client.configuration.pac;

/**
 * Created on 2018-12-11 14:26
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public abstract class SpacFilter {

  public abstract boolean filter(String host);
}
