/*
 * Copyright [2018] [IoooTech,Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package tech.iooo.coco.core.handler;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.socksx.v5.DefaultSocks5CommandRequest;
import io.netty.handler.codec.socksx.v5.DefaultSocks5CommandResponse;
import io.netty.handler.codec.socksx.v5.Socks5AddressType;
import io.netty.handler.codec.socksx.v5.Socks5CommandResponse;
import io.netty.handler.codec.socksx.v5.Socks5CommandStatus;
import io.netty.handler.codec.socksx.v5.Socks5CommandType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.netty.handler.NettyAdaptiveHelper;

/**
 * Created on 2018/10/30 7:30 PM
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=miroku">Ivan97</a>
 */
@Sharable
public class Socks5CommandRequestHandler extends SimpleChannelInboundHandler<DefaultSocks5CommandRequest> {

  private static final Logger logger = LoggerFactory.getLogger(Socks5CommandRequestHandler.class);
  private EventLoopGroup bossGroup;

  public Socks5CommandRequestHandler(EventLoopGroup bossGroup) {
    this.bossGroup = bossGroup;
  }

  @Override
  protected void channelRead0(final ChannelHandlerContext clientChannelContext, DefaultSocks5CommandRequest msg) throws Exception {
    if (logger.isDebugEnabled()) {
      logger.debug("目标服务器  : " + msg.type() + "," + msg.dstAddr() + "," + msg.dstPort());
    }
    if (msg.type().equals(Socks5CommandType.CONNECT)) {
      if (logger.isDebugEnabled()) {
        logger.debug("准备连接目标服务器");
      }
      Bootstrap bootstrap = new Bootstrap();
      bootstrap.group(bossGroup)
          .channel(NettyAdaptiveHelper.clientSocketChannel)
          .option(ChannelOption.TCP_NODELAY, true)
          .handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
              //ch.pipeline().addLast(new LoggingHandler());//in out
              //将目标服务器信息转发给客户端
              ch.pipeline().addLast(new Dest2ClientHandler(clientChannelContext));
            }
          });
      if (logger.isDebugEnabled()) {
        logger.debug("连接目标服务器");
      }
      ChannelFuture future = bootstrap.connect(msg.dstAddr(), msg.dstPort());
      future.addListener(new ChannelFutureListener() {
        @Override
        public void operationComplete(final ChannelFuture future) throws Exception {
          if (future.isSuccess()) {
            if (logger.isDebugEnabled()) {
              logger.debug("成功连接目标服务器");
            }
            clientChannelContext.pipeline().addLast(new Client2DestHandler(future));
            Socks5CommandResponse commandResponse = new DefaultSocks5CommandResponse(Socks5CommandStatus.SUCCESS, Socks5AddressType.IPv4);
            clientChannelContext.writeAndFlush(commandResponse);
          } else {
            Socks5CommandResponse commandResponse = new DefaultSocks5CommandResponse(Socks5CommandStatus.FAILURE, Socks5AddressType.IPv4);
            clientChannelContext.writeAndFlush(commandResponse);
          }
        }

      });
    } else {
      clientChannelContext.fireChannelRead(msg);
    }
  }

  /**
   * 将目标服务器信息转发给客户端
   *
   * @author huchengyi
   */
  private static class Dest2ClientHandler extends ChannelInboundHandlerAdapter {

    private ChannelHandlerContext clientChannelContext;

    public Dest2ClientHandler(ChannelHandlerContext clientChannelContext) {
      this.clientChannelContext = clientChannelContext;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx2, Object destMsg) throws Exception {
      if (logger.isDebugEnabled()) {
        logger.debug("将目标服务器信息转发给客户端");
      }
      clientChannelContext.writeAndFlush(destMsg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx2) throws Exception {
      if (logger.isDebugEnabled()) {
        logger.debug("目标服务器断开连接");
      }
      clientChannelContext.channel().close();
    }
  }

  /**
   * 将客户端的消息转发给目标服务器端
   *
   * @author huchengyi
   */
  private static class Client2DestHandler extends ChannelInboundHandlerAdapter {

    private ChannelFuture destChannelFuture;

    public Client2DestHandler(ChannelFuture destChannelFuture) {
      this.destChannelFuture = destChannelFuture;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
      if (logger.isDebugEnabled()) {
        logger.debug("将客户端的消息转发给目标服务器端");
      }
      destChannelFuture.channel().writeAndFlush(msg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
      if (logger.isDebugEnabled()) {
        logger.debug("客户端断开连接");
      }
      destChannelFuture.channel().close();
    }
  }
}
