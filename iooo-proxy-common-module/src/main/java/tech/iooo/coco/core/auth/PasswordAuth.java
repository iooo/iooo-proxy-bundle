package tech.iooo.coco.core.auth;

/**
 * Created on 2018-12-06 21:08
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
public interface PasswordAuth extends Auth {

  boolean auth(String password);
}
