/*
 * Copyright [2018] [IoooTech,Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package tech.iooo.coco.core.handler;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import lombok.Getter;
import tech.iooo.coco.core.handler.logging.ProxyLogging;
import tech.iooo.coco.core.listener.ChannelListener;

/**
 * Created on 2018/10/30 7:25 PM
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=miroku">Ivan97</a>
 */
@Sharable
public class ProxyChannelTrafficShapingHandler extends ChannelTrafficShapingHandler {

  public static final String PROXY_TRAFFIC = "ProxyChannelTrafficShapingHandler";

  @Getter
  private long beginTime;
  @Getter
  private long endTime;
  @Getter
  private String username = "anonymous";

  private ProxyLogging proxyLogging;

  private ChannelListener channelListener;

  public ProxyChannelTrafficShapingHandler(long checkInterval, ProxyLogging proxyLogging, ChannelListener channelListener) {
    super(checkInterval);
    this.proxyLogging = proxyLogging;
    this.channelListener = channelListener;
  }

  public static ProxyChannelTrafficShapingHandler get(ChannelHandlerContext ctx) {
    return (ProxyChannelTrafficShapingHandler) ctx.pipeline().get(PROXY_TRAFFIC);
  }

  public static void username(ChannelHandlerContext ctx, String username) {
    get(ctx).username = username;
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    beginTime = System.currentTimeMillis();
    if (channelListener != null) {
      channelListener.active(ctx);
    }
    super.channelActive(ctx);
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    endTime = System.currentTimeMillis();
    if (channelListener != null) {
      channelListener.inActive(ctx);
    }
    proxyLogging.log(ctx);
    super.channelInactive(ctx);
  }
}
