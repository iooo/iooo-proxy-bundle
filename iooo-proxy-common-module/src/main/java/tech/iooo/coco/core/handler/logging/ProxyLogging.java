/*
 * Copyright [2018] [IoooTech,Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package tech.iooo.coco.core.handler.logging;

import io.netty.channel.ChannelHandlerContext;
import java.net.InetSocketAddress;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.boot.core.utils.NetUtils;
import tech.iooo.coco.core.configuration.Constants;
import tech.iooo.coco.core.handler.ProxyChannelTrafficShapingHandler;

/**
 * Created on 2018/10/30 7:19 PM
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=miroku">Ivan97</a>
 */
public class ProxyLogging {

  private static final Logger logger = LoggerFactory.getLogger(ProxyLogging.class);

  public void log(ChannelHandlerContext ctx) {
    ProxyChannelTrafficShapingHandler trafficShapingHandler = ProxyChannelTrafficShapingHandler.get(ctx);
    InetSocketAddress localAddress = (InetSocketAddress) ctx.channel().localAddress();
    InetSocketAddress remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();

    long readByte = trafficShapingHandler.trafficCounter().cumulativeReadBytes();
    long writeByte = trafficShapingHandler.trafficCounter().cumulativeWrittenBytes();

    logger.info("username:[{}],begin:[{}],end:[{}],local-[{}:{}],remote-[{}:{}],read:[{}],write:[{}],total:[{}]",
        trafficShapingHandler.getUsername(),
        resolve(trafficShapingHandler.getBeginTime()),
        resolve(trafficShapingHandler.getEndTime()),
        NetUtils.getLocalHost(),
        localAddress.getPort(),
        remoteAddress.getAddress().getHostAddress(),
        remoteAddress.getPort(),
        readByte,
        writeByte,
        (readByte + writeByte));
  }

  private String resolve(long time) {
    long epochSecond = time / 1000;
    long tail = time - epochSecond * 1000;
    return LocalDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT)) + "." + tail;
  }
}
