package tech.iooo.coco.core.configuration;

import lombok.experimental.UtilityClass;

/**
 * Created on 2018-12-06 22:30
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@UtilityClass
public class Constants {

  public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
