package tech.iooo.coco.server.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created on 2018-12-06 13:43
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Data
@ConfigurationProperties(prefix = "proxy")
public class ProxyProperties {

  private int port = 1080;
  private boolean auth = true;
  private String username = "username";
  private String password = "password";
}
