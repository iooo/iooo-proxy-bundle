/*
 * Copyright [2018] [IoooTech,Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package tech.iooo.coco.server.handler;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.socksx.v5.DefaultSocks5PasswordAuthRequest;
import io.netty.handler.codec.socksx.v5.DefaultSocks5PasswordAuthResponse;
import io.netty.handler.codec.socksx.v5.Socks5PasswordAuthResponse;
import io.netty.handler.codec.socksx.v5.Socks5PasswordAuthStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.coco.core.auth.UsernamePasswordAuth;
import tech.iooo.coco.core.handler.ProxyChannelTrafficShapingHandler;

/**
 * Created on 2018/10/30 9:05 PM
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=miroku">Ivan97</a>
 */
@Sharable
public class Socks5PasswordAuthRequestHandler extends SimpleChannelInboundHandler<DefaultSocks5PasswordAuthRequest> {

  private static final Logger logger = LoggerFactory.getLogger(Socks5PasswordAuthRequestHandler.class);

  private final UsernamePasswordAuth usernamePasswordAuth;

  public Socks5PasswordAuthRequestHandler(UsernamePasswordAuth usernamePasswordAuth) {
    this.usernamePasswordAuth = usernamePasswordAuth;
  }

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, DefaultSocks5PasswordAuthRequest msg) throws Exception {
    if (logger.isDebugEnabled()) {
      logger.debug("用户名密码 : " + msg.username() + "," + msg.password());
    }
    if (usernamePasswordAuth.auth(msg.username(), msg.password())) {
      ProxyChannelTrafficShapingHandler.username(ctx, msg.username());
      Socks5PasswordAuthResponse passwordAuthResponse = new DefaultSocks5PasswordAuthResponse(Socks5PasswordAuthStatus.SUCCESS);
      ctx.writeAndFlush(passwordAuthResponse);
    } else {
      ProxyChannelTrafficShapingHandler.username(ctx, "unauthorized");
      Socks5PasswordAuthResponse passwordAuthResponse = new DefaultSocks5PasswordAuthResponse(Socks5PasswordAuthStatus.FAILURE);
      //发送鉴权失败消息，完成后关闭channel
      ctx.writeAndFlush(passwordAuthResponse).addListener(ChannelFutureListener.CLOSE);
    }
  }
}
