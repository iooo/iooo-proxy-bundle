package tech.iooo.coco.server.lifecycle;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.socksx.SocksPortUnificationServerHandler;
import io.netty.handler.codec.socksx.v5.Socks5CommandRequestDecoder;
import io.netty.handler.codec.socksx.v5.Socks5PasswordAuthRequestDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import java.net.InetSocketAddress;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Service;
import tech.iooo.boot.netty.handler.NettyAdaptiveHelper;
import tech.iooo.coco.core.auth.UsernamePasswordAuth;
import tech.iooo.coco.core.handler.ProxyChannelTrafficShapingHandler;
import tech.iooo.coco.core.handler.ProxyIdleHandler;
import tech.iooo.coco.core.handler.Socks5CommandRequestHandler;
import tech.iooo.coco.core.handler.logging.ProxyLogging;
import tech.iooo.coco.core.listener.ChannelListener;
import tech.iooo.coco.server.configuration.ProxyProperties;
import tech.iooo.coco.server.handler.Socks5InitialRequestHandler;
import tech.iooo.coco.server.handler.Socks5PasswordAuthRequestHandler;

/**
 * Created on 2018-12-06 13:41
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Service
public class ProxyServerLifecycle implements SmartLifecycle {

  private static final Logger logger = LoggerFactory.getLogger(ProxyServerLifecycle.class);

  @Autowired
  private ProxyProperties proxyProperties;
  @Autowired
  private Socks5InitialRequestHandler socks5InitialRequestHandler;
  @Autowired
  private UsernamePasswordAuth usernamePasswordAuth;

  private boolean running;
  private EventLoopGroup bossGroup = NettyAdaptiveHelper.bossEventLoopGroup;
  private EventLoopGroup workerGroup = NettyAdaptiveHelper.clientEventLoopGroup(5);

  private ChannelListener channelListener;

  @Override
  @SneakyThrows
  public void start() {
    logger.info("=================ProxyServer start=================");

    ServerBootstrap serverBootstrap = new ServerBootstrap();
    serverBootstrap.group(bossGroup, workerGroup)
        .channel(NettyAdaptiveHelper.serverSocketChannel)
        .option(ChannelOption.SO_BACKLOG, 128)
        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1000 * 5)
        .localAddress(new InetSocketAddress(proxyProperties.getPort()))
        .childHandler(new ChannelInitializer<SocketChannel>() {
          @Override
          protected void initChannel(SocketChannel ch) {
            ChannelPipeline channelPipeline = ch.pipeline();
            //流量统计
            channelPipeline.addLast(
                ProxyChannelTrafficShapingHandler.PROXY_TRAFFIC,
                new ProxyChannelTrafficShapingHandler(3000, new ProxyLogging(), channelListener)
            );
            //channel超时处理
            channelPipeline.addLast(new IdleStateHandler(3, 30, 0));
            channelPipeline.addLast(new ProxyIdleHandler());

            //日志
            channelPipeline.addLast("logger", new LoggingHandler(LogLevel.DEBUG));

            channelPipeline.addLast(new SocksPortUnificationServerHandler());
            //sock5 init
            channelPipeline.addLast(socks5InitialRequestHandler);
            if (proxyProperties.isAuth()) {
              //socks auth
              channelPipeline.addLast(new Socks5PasswordAuthRequestDecoder());
              //socks auth
              channelPipeline.addLast(new Socks5PasswordAuthRequestHandler(usernamePasswordAuth));
            }
            //socks connection
            channelPipeline.addLast(new Socks5CommandRequestDecoder());
            //Socks connection
            channelPipeline.addLast(new Socks5CommandRequestHandler(bossGroup));
          }
        });
    ChannelFuture future = serverBootstrap.bind().sync().addListener(result -> {
      if (result.isSuccess()) {
        logger.info("services started on port {}", proxyProperties.getPort());
      }
    });
    future.channel().closeFuture().sync();

    this.running = true;
  }

  @Override
  public void stop() {
    bossGroup.shutdownGracefully().syncUninterruptibly();
    workerGroup.shutdownGracefully().syncUninterruptibly();
    this.running = false;
  }

  @Override
  public boolean isRunning() {
    return this.running;
  }
}
