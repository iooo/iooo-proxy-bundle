package tech.iooo.coco.server.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created on 2018-12-06 13:43
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-proxy-bundle">Ivan97</a>
 */
@Configuration
@EnableConfigurationProperties(ProxyProperties.class)
public class ProxyConfiguration {

}
